# Source from https://registry.terraform.io/modules/oracle-terraform-modules/vcn/oci/
module "vcn" {
  source  = "oracle-terraform-modules/vcn/oci"
  version = "3.1.0"

  compartment_id = oci_identity_compartment.this.id
  region         = var.region

  internet_gateway_route_rules = null
  local_peering_gateways       = null
  nat_gateway_route_rules      = null

  vcn_name      = "oci-devops-hello-vcn"
  vcn_dns_label = "ocidevopshello"
  vcn_cidrs     = ["10.100.0.0/16"]

  create_internet_gateway = true
  create_service_gateway  = false
}
