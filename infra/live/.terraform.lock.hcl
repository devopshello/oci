# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "3.10.1"
  constraints = "3.10.1"
  hashes = [
    "h1:Xlgu+XCoky8JVkxGBURRh0DrMldhdUDxdpakcKynEz4=",
    "zh:0e4914c44984bdfc11140198950cbabe09fada993035f1cdf4a55b3e987ab3cc",
    "zh:1671345b9b0d6f65ed14242e7ff0f1792e6fc2fc4c1df1c1526818882c45b9af",
    "zh:1796f31caf0719b0bd65ad1b8ed8711b3942162e5c50a0bd4653332986b930cc",
    "zh:247c6d4a60f4bc595a11d323ea5c3720eb752c4db380e5a8be67c97c03744a22",
    "zh:479a4a1a332aa44aac706c59244c90d83dcaca08feacec81633c2316d1aba1df",
    "zh:53927fbf617a6234db12d74b0f970a65cca004ea13def2138b43286965d40e43",
    "zh:73d0277597b7ccec261538ee49b1ddb1118fe28cf626df43b5e09abbffe6aa5d",
    "zh:7431e3f2d0c254cc3f77bc62ad1eefb8ae7ee52d0de7b28ac90b6a98c67694ea",
    "zh:98d859f6e510ddbe819ffb1c1d6280876298645dd0f74d25f6ed320dc4772b95",
    "zh:c219569db8de5f7b5a402182599a32207dec0fef35c950c7594ceee3e95c4c94",
    "zh:c312da4780bf776e3ef18c9e719b0a2dd0656f047a7ef5058dda3a3a931bb8d8",
    "zh:c752fa6860b3ccbec0ee359cfad805d6c6205366c5889341dc6ee6cb6bef0bdd",
    "zh:cd9f3d4f480a262d3e4eb663fcdfbc788ea6d8063beca19eed26e962c2df3725",
    "zh:f569573661f68e72e9044b8cf76ee40f652b19c788c0e26baf52bfbf84c9f26f",
  ]
}

provider "registry.terraform.io/hashicorp/oci" {
  version = "4.68.0"
  hashes = [
    "h1:bfj2Sj01ZRGFFCXJt2eBcjDYHVVa3fw0U4+jnUiQJHM=",
    "zh:0eb113292d8c0dfb83af8b57603f3d87f539e3d6fd59101cdc90cb839a7a15d2",
    "zh:38e135a2b40c4a72f21bfaa9761e5d541095726dbbd6c0e68b599f14626f4d15",
    "zh:42034e5dccb0c8067390242f25c10c41da68afa6bfd5dfa8a29bacd7cf0acc72",
    "zh:51e12d9aa6ae9fa2925d98e5b30249464ee11a0b79bbdf696bbb7d2a505b6e6c",
    "zh:54b5bf9589b3ab0527f74c3054dd28dbb427fb3868d65388d2984cf2c7637a76",
    "zh:55d017747528964b476c73068f4e0a419e58721eb2f654c10947d6756a66cf63",
    "zh:6d5ef791501fd20e6a5cf8b1a674c5de2c30cb2219c54b619cc42d8da69739a5",
    "zh:7bd75323793a6974d0e262a3e256d8ab6e323fdeca5c344a6ee4d2582a8e5ebe",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:a381b0ae5bc8ccecaf8e3270e41e220e7f900052e2f34928d81e66e335f5afc6",
    "zh:bbdcc854328cad0402534866d75b11275ea7dbef4d43e0ff4955c2e9782a40d2",
    "zh:c1cf442599f7d87dcba638f291babd786c1384603a57af7b9525bfca9ce86a79",
    "zh:c5f3c18b0ad41536cf6004a5b5d367bc3d28be6edaccd1cea133391a9d3b9424",
    "zh:d5cd4b7e621ee421a119cc477cea6b0ff4d0619c78e997104388801711670365",
    "zh:d8870c7bbce64613f9318f7251b803b7d018d6bd0757623c02ca2ebe0807f12f",
  ]
}
