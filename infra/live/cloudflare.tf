resource "cloudflare_record" "foobar" {
  zone_id         = data.cloudflare_zone.this.id
  value           = oci_core_instance.ubuntu_instance.public_ip
  name            = "hellooci"
  type            = "A"
  ttl             = 1
  proxied         = true
  allow_overwrite = true
}

data "cloudflare_zone" "this" {
  name = "leolleo.dev"
}
