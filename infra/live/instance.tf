resource "oci_core_instance" "ubuntu_instance" {
  availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
  compartment_id      = oci_identity_compartment.this.id
  shape               = "VM.Standard.E2.1.Micro"

  # Canonical-Ubuntu-20.04-2022.02.15-0
  source_details {
    source_id   = "ocid1.image.oc1.sa-saopaulo-1.aaaaaaaaginwdzaorpkaqbitqt37gdfxk6qagjcpv6pjqfdwayhqasg3mioq"
    source_type = "image"
  }

  display_name = "nginx-rproxy"

  create_vnic_details {
    assign_public_ip = true
    subnet_id        = oci_core_subnet.public.id
  }

  metadata = {
    ssh_authorized_keys = file("./files/oci.pub")
  }

  preserve_boot_volume = false
}
