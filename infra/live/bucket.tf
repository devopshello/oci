resource "oci_objectstorage_bucket" "this" {
  compartment_id = oci_identity_compartment.this.id
  namespace      = data.oci_objectstorage_namespace.this.namespace
  name           = "oci-devops-hello"

  access_type = "ObjectRead"
}

data "oci_objectstorage_namespace" "this" {
  compartment_id = var.tenancy_ocid
}
