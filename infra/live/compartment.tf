resource "oci_identity_compartment" "this" {
  compartment_id = var.tenancy_ocid
  description    = "OCI DevOps Hello World"
  name           = "oci-devops-hello"
}
